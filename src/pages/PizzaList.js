import Component from '../components/Component.js';
import PizzaThumbnail from '../components/PizzaThumbnail.js';

import Page from './Page';
export default class PizzaList extends Page {
	#pizzas;

	constructor(pizzas) {
		super('pizzaList'); // on pase juste la classe CSS souhaitée
		this.pizzas = pizzas;
	}
	//.. suite de la classe

	set pizzas(value) {
		this.#pizzas = value;
		this.children = this.#pizzas.map(pizza => new PizzaThumbnail(pizza));
	}
}
