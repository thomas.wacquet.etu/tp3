import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		document
			.querySelector('.pizzaForm')
			.addEventListener('submit', this.submit);
		super.mount(element);
	}

	submit(event) {
		event.preventDefault();
		const input = document.querySelector('input[name=name]');
		const name = input.value;
		if (!name) {
			alert('erreur de saisie');
			return;
		}

		alert(`La pizza ${name} a été ajoutée`);
		input.value = '';
	}
}
