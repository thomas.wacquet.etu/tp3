import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

const mainMenuActiveLink = document.querySelector(
	'.mainMenu li:nth-child(1) a'
);

mainMenuActiveLink.setAttribute(
	'class',
	`${mainMenuActiveLink.getAttribute('href')} active`
);

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

pizzaList.pizzas = data;
Router.menuElement = document.querySelector('.mainMenu');
Router.navigate(document.location.pathname); // affiche la liste des pizzas

// console.log(
// 	document.querySelector('.pizzaThumbnail:nth-child(2) h4').innerHTML
// );

document.querySelector(
	'.logo'
).innerHTML += `<small>les pizzas c'est la vie</small>`;

console.log(
	document.querySelector('footer a:nth-child(2)').getAttribute('href')
);

const newsContainer = document.querySelector('.newsContainer');
newsContainer.setAttribute('style', '');
newsContainer.querySelector('.closeButton').addEventListener('click', () => {
	newsContainer.setAttribute('style', 'display:none');
});

window.onpopstate = () => {
	Router.navigate(document.location.pathname);
};
