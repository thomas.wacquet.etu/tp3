export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];
	static #menuElement;

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);

			this.#menuElement.querySelector('.active').setAttribute('class', '');

			const link = this.#menuElement.querySelector(`a[href='${path}']`);
			link.setAttribute('class', `${link.getAttribute('class')} active`);

			history.pushState(null, null, path);
		}
	}

	static set menuElement(element) {
		this.#menuElement = element;
		// au clic sur n'importe quel lien contenu dans "element"
		// déclenchez un appel à Router.navigate(path)
		// où "path" est la valeur de l'attribut `href=".."` du lien cliqué
		const links = this.#menuElement.querySelectorAll('a');
		links.forEach(link => {
			link.addEventListener('click', e => {
				e.preventDefault();
				const href = e.currentTarget.getAttribute('href');
				this.navigate(href);
			});
		});
	}
}
